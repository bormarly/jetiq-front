import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {User} from '../../shared/interfaces/auth-interfaces';
import {AuthService} from '../../shared/services/auth.service';
import {MaterialService} from '../../shared/services/material.service';


@Component({
    selector: 'app-register-page',
    templateUrl: './register-page.component.html',
    styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit, OnDestroy {

    form: FormGroup
    registerSub: Subscription

    constructor(
        private auth: AuthService,
        private router: Router,
    ) { }


    ngOnInit(): void {
        this.form = new FormGroup({
            username: new FormControl(null, [Validators.required]),
            email: new FormControl(null, [Validators.required, Validators.email]),
            password: new FormControl(null, [Validators.required]),
            password2: new FormControl(null, [Validators.required]),
            jetiqUsername: new FormControl(null, [Validators.required]),
            jetiqPassword: new FormControl(null, [Validators.required]),
        })
    }

    ngOnDestroy(): void {
        if (this.registerSub) {
            this.registerSub.unsubscribe()
        }
    }

    get isPasswordEqual(): boolean {
        return this.form.value.password === this.form.value.password2
    }

    submit() {

        this.form.disable()

        const user: User = {
            username: this.form.value.username,
            email: this.form.value.email,
            password: this.form.value.password,
            jetiq_username: this.form.value.jetiqUsername,
            jetiq_password: this.form.value.jetiqPassword,
        }

        this.registerSub = this.auth.register(user).subscribe(
            () => {
                this.router.navigate(['/login'])
                MaterialService.toast('Зареєстровано!')
                MaterialService.toast('Тепер можете увійти в систему')
            },
            error => {
                this.form.enable()
                MaterialService.toast('Сталася момилка')
                console.log(error)
                if (typeof error.error.detail === 'string') {
                    MaterialService.toast(error.error.detail)
                }

            }
        )

    }
}
