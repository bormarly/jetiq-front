import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {AuthService} from '../../shared/services/auth.service';
import {MaterialService} from '../../shared/services/material.service';


@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, OnDestroy {

    form: FormGroup
    loginSub: Subscription


    constructor(
        private auth: AuthService,
        private router: Router,
        private route: ActivatedRoute,
    ) { }

    ngOnInit(): void {

        this.form = new FormGroup({
            username: new FormControl(null, Validators.required),
            password: new FormControl(null, Validators.required),
        })

        this.route.queryParams.subscribe(
            params => {
                if (params['accessDenied']) {
                    MaterialService.toast('Авторизуйтесь')
                }
            }
        )

    }

    submit() {

        this.form.disable()

        const formData: FormData = new FormData()
        formData.append('username', this.form.value.username)
        formData.append('password', this.form.value.password)


        this.loginSub = this.auth.login(formData).subscribe(
            () => this.router.navigate(['/overview']),
            err => {
                console.log(err)
                MaterialService.toast(err.error.detail)
                this.form.enable()
            },
        )
    }

    ngOnDestroy(): void {

        if (this.loginSub) {
            this.loginSub.unsubscribe()
        }

    }
}
