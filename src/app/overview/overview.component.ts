import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';

import {User} from '../shared/interfaces/auth-interfaces';
import {AuthService} from '../shared/services/auth.service';
import {LessonService} from '../shared/services/lesson.service';
import {LessonInDB} from '../shared/interfaces/schedule-interfaces';
import {getCurrentWeekDay} from '../shared/conts';

@Component({
    selector: 'app-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

    currentUser$: Observable<User>
    lessons$: Observable<LessonInDB[]>

    constructor(
        private auth: AuthService,
        private lessonService: LessonService,
    ) { }

    ngOnInit(): void {
        this.currentUser$ = this.auth.getCurrentUser()
        const weekday = getCurrentWeekDay().value
        this.lessons$ = this.lessonService.getAll(weekday)
    }

}
