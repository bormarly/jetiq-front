import { Component, OnInit } from '@angular/core'
import {FormControl, FormGroup, Validators} from '@angular/forms'
import {ScheduleSubjectService} from '../../shared/services/schedule-subject.service'
import { ScheduleSubject } from '../../shared/interfaces/schedule-interfaces'
import {MaterialService} from '../../shared/services/material.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';

@Component({
    selector: 'app-subject-form',
    templateUrl: './subject-form.component.html',
    styleUrls: ['./subject-form.component.scss']
})
export class SubjectFormComponent implements OnInit {

    form: FormGroup
    isNew: boolean = true
    subject: ScheduleSubject

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private scheduleSubjectService: ScheduleSubjectService,
    ) { }

    ngOnInit(): void {

        this.form = new FormGroup({
            name: new FormControl(null, Validators.required),
            teacher: new FormControl(null, Validators.required),
            meet: new FormControl(null, Validators.required),
        })

        this.form.disable()

        this.route.params.pipe(
            switchMap((params: Params) => {
                if (params['name']) {
                    this.isNew = false
                    return this.scheduleSubjectService.getByName(params['name'])
                }
                return of(null)
            })
        ).subscribe(
            (subject: ScheduleSubject) => {
                if (subject) {
                    this.subject = subject
                    this.form.patchValue({
                        name: subject.name,
                        teacher: subject.teacher,
                        meet: subject.meet_url_name,
                    })
                }
                this.form.enable()
            },
            error => MaterialService.toast(error.error.detail)
        )

    }

    submit() {

        let obs$: Observable<ScheduleSubject>

        this.form.disable()

        const subject: ScheduleSubject =  {
            name: this.form.value.name,
            teacher: this.form.value.teacher,
            meet_url_name: this.form.value.meet,
        }

        if (this.isNew) {
            obs$ = this.scheduleSubjectService.create(subject)
        } else {
            obs$ = this.scheduleSubjectService.update(this.subject.name, subject)
        }

        obs$.subscribe(
            () => MaterialService.toast('Subjects has been saved.'),
            error => {
                console.log(error)
                MaterialService.toast(error.error.detail)
                this.form.enable()
            },
            () => {
                this.form.enable()
                this.subject = subject
                this.router.navigate(['/subject'])

            }
        )


    }

    delete () {
        this.scheduleSubjectService.delete(this.subject.name).subscribe(
            () => MaterialService.toast('Deleted'),
            error => MaterialService.toast(error.error.detail),
            () => this.router.navigate(['/subject'])
        )
    }

    addLesson() {
        this.router.navigate(['/lesson/new'], {
            queryParams: {
                subjectName: this.subject.name
            },
        })
    }
}
