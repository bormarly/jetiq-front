import {Observable} from 'rxjs'
import { Component, OnInit } from '@angular/core'

import {ScheduleSubject} from '../shared/interfaces/schedule-interfaces'
import {ScheduleSubjectService} from '../shared/services/schedule-subject.service'


@Component({
    selector: 'app-subject',
    templateUrl: './subject-page.component.html',
    styleUrls: ['./subject-page.component.scss']
})
export class SubjectPageComponent implements OnInit {

    subjects$: Observable<ScheduleSubject[]>

    constructor(private scheduleSubjectService: ScheduleSubjectService) { }

    ngOnInit(): void {
        this.subjects$ = this.scheduleSubjectService.getAll()
    }

}
