import { NgModule } from '@angular/core'
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser'

import { AppComponent } from './app.component'
import { AppRoutingModule } from './app-routing.module'
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { SubjectPageComponent } from './subject-page/subject-page.component'
import { LoaderComponent } from './shared/components/loader/loader.component'
import { SubjectFormComponent } from './subject-page/subject-form/subject-form.component'
import { BaseLayoutComponent } from './shared/components/base-layout/base-layout.component';
import { LessonPageComponent } from './lesson-page/lesson-page.component';
import { LessonFormComponent } from './lesson-page/lesson-form/lesson-form.component';
import { LessonDetailPageComponent } from './lesson-page/lesson-detail-page/lesson-detail-page.component';
import { LessonVisitsComponent } from './lesson-page/lesson-detail-page/lesson-visits/lesson-visits.component';
import { WeekdayPipe } from './shared/pipes/weekday.pipe';
import { LessonFilterComponent } from './lesson-page/lesson-filter/lesson-filter.component';
import { LoginPageComponent } from './auth/login-page/login-page.component'
import {TokenInterceptor} from './shared/interceptors/token.interceptor';
import {TitleCasePipe} from '@angular/common';
import { RegisterPageComponent } from './auth/register-page/register-page.component';
import { LessonItemComponent } from './lesson-page/lesson-item/lesson-item.component';
import { OverviewComponent } from './overview/overview.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { WeekdayInputComponent } from './shared/components/weekday-input/weekday-input.component';
import { MediaURLPipe } from './shared/pipes/meida-url.pipe';
import { MaterialBoxComponent } from './shared/components/material-box/material-box.component'

@NgModule({
    declarations: [
        AppComponent,
        BaseLayoutComponent,
        SubjectPageComponent,
        LoaderComponent,
        SubjectFormComponent,
        LessonPageComponent,
        LessonFormComponent,
        LessonDetailPageComponent,
        LessonVisitsComponent,
        WeekdayPipe,
        LessonFilterComponent,
        LoginPageComponent,
        RegisterPageComponent,
        LessonItemComponent,
        OverviewComponent,
        ProfilePageComponent,
        WeekdayInputComponent,
        MediaURLPipe,
        MaterialBoxComponent,
    ],
    imports: [
        // lib
        ClipboardModule,
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule,
        // app
        AppRoutingModule,
        FormsModule,
    ],
    providers: [
        {
            multi: true,
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
        },
        TitleCasePipe,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
