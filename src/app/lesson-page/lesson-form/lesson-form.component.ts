import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';

import {Lesson, ScheduleSubject} from '../../shared/interfaces/schedule-interfaces';
import {ScheduleSubjectService} from '../../shared/services/schedule-subject.service';
import {MaterialAutocomplete, MaterialInstance, MaterialService, MaterialTimePicker} from '../../shared/services/material.service';
import {LessonService} from '../../shared/services/lesson.service';
import {weekDaysList} from '../../shared/conts';
import {WeekDayOption} from '../../shared/interfaces/common-interfaces';


@Component({
    selector: 'app-lesson-form',
    templateUrl: './lesson-form.component.html',
    styleUrls: ['./lesson-form.component.scss']
})
export class LessonFormComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild('slugRef') slugRef: ElementRef
    @ViewChild('timeRef') timeRef: ElementRef
    @ViewChild('subjectRef') subjectRef: ElementRef
    @ViewChild('weekdayRef') weekdayRef: ElementRef

    form: FormGroup

    timeM: MaterialTimePicker
    slugM: MaterialAutocomplete
    subjectM: MaterialInstance

    lessonsSub: Subscription
    subjectsSub: Subscription

    weekday: string
    slugs: string[]
    subjects: ScheduleSubject[]

    weekDays: WeekDayOption[] = weekDaysList

    constructor(
        private scheduleSubjectService: ScheduleSubjectService,
        private lessonService: LessonService,
        private router: Router,
        private route: ActivatedRoute,
    ) { }

    ngOnInit(): void {
        this.form = new FormGroup({
            subject: new FormControl(null, Validators.required),
            slug: new FormControl(null, Validators.required),
            time: new FormControl(null, Validators.required),
        })

        this.route.queryParams.subscribe(params => {
            if (params['subjectName']) {
                this.form.patchValue({
                    subject: params['subjectName']
                })
            }
        })

    }

    ngAfterViewInit(): void {
        this.subjectsSub = this.scheduleSubjectService.getAll().subscribe(
            subjects => {
                this.subjects = subjects
                setTimeout(() => {
                    this.subjectM = MaterialService.initFormSelect(this.subjectRef)
                }, 50)
            },
            error => MaterialService.toast(error.error.detail),
        )

        this.lessonsSub = this.lessonService.getAll().subscribe(
            lessons => {
                const slugs = [... new Set(lessons.map(l => l.week_slug))]
                this.slugM = MaterialService.initAutocomplete(this.slugRef, slugs, () => {
                    this.form.patchValue({
                        slug: this.slugM.el.value,
                    })
                })
            },
            error => MaterialService.toast(error.error.detail),
        )


        this.timeM = MaterialService.initTimePicker(this.timeRef, this.onCloseTimePicker.bind(this))
    }

    ngOnDestroy(): void {
        this.timeM.destroy()
        this.slugM.destroy()
        this.subjectM.destroy()
        this.lessonsSub.unsubscribe()
        this.subjectsSub.unsubscribe()
    }

    onCloseTimePicker() {
        console.log(this.timeM.time)
        this.form.patchValue({
            time: this.timeM.time
        })
    }

    submit() {
        this.form.disable()

        console.log(this.slugM.activeIndex)
        console.log(this.slugM)

        const lesson: Lesson = {
            subject_name: this.form.value.subject,
            week_slug: this.form.value.slug,
            time: this.form.value.time,
            weekday: this.weekday,
        }

        this.lessonService.create(lesson).subscribe(
            lesson => {
                MaterialService.toast('Збережено')
                this.form.enable()
                this.router.navigate(['/lesson', lesson.id])
            },
            error => MaterialService.toast(error.error.detail),
        )

    }

    choseWeekday(weekday: string) {
        this.weekday = weekday
    }
}
