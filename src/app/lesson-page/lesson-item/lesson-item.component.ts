import {Component, Input, OnInit} from '@angular/core';

import {LessonInDB} from '../../shared/interfaces/schedule-interfaces';

@Component({
  selector: 'app-lesson-item',
  templateUrl: './lesson-item.component.html',
  styleUrls: ['./lesson-item.component.scss']
})
export class LessonItemComponent implements OnInit {

    @Input('lesson') lesson: LessonInDB

    constructor() { }

    ngOnInit(): void {
    }

}
