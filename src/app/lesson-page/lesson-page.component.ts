import {Observable} from 'rxjs';
import { Component, OnInit } from '@angular/core';

import {Filter} from './lesson-filter/filter';
import {LessonService} from '../shared/services/lesson.service';
import {LessonInDB, WeekDays} from '../shared/interfaces/schedule-interfaces';
import {ActivatedRoute, Params, Router} from '@angular/router';


@Component({
    selector: 'app-lesson-page',
    templateUrl: './lesson-page.component.html',
    styleUrls: ['./lesson-page.component.scss']
})
export class LessonPageComponent implements OnInit {

    lessons$: Observable<LessonInDB[]>
    isFilerVisible: boolean = false
    weekday: string | WeekDays
    weekSlug: string

    constructor(
        private lessonService: LessonService,
        private route: ActivatedRoute,
        private router: Router,
    ) { }

    ngOnInit(): void {
        this.route.queryParams.subscribe(
            params => {
                if (params['weekday']) {
                    this.weekday = params['weekday']
                }
                if (params['weekSlug']) {
                    this.weekSlug = params['weekSlug']
                }
                this.fetch()
            }
        )
    }

    applyFilter(filter: Filter) {
        this.weekday = filter.weekday
        this.weekSlug = filter.weekSlug
        this.fetch()

        let params: Params = {}

        if (this.weekday) {
            params['weekday'] = this.weekday
        }
        if (this.weekSlug) {
            params['weekSlug'] = this.weekSlug
        }
        this.router.navigate([], {
            relativeTo: this.route,
            queryParams: params,
        })

    }

    private fetch() {
        this.lessons$ = this.lessonService.getAll(this.weekday, this.weekSlug)
    }

}
