import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable, Subscriber, Subscription} from 'rxjs';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {formatDate} from '@angular/common';

import {weekDaysList} from '../../shared/conts';
import {Visit} from '../../shared/interfaces/visit-interfaces';
import {VisitService} from '../../shared/services/visit.service';
import {LessonService} from '../../shared/services/lesson.service';
import {WeekDayOption} from '../../shared/interfaces/common-interfaces';
import {ScheduleSubjectService} from '../../shared/services/schedule-subject.service';
import {Lesson, LessonInDB, ScheduleSubject} from '../../shared/interfaces/schedule-interfaces';
import {MaterialDatePicker, MaterialInstance, MaterialService, MaterialTimePicker} from '../../shared/services/material.service';


@Component({
    selector: 'app-lesson-detail-page',
    templateUrl: './lesson-detail-page.component.html',
    styleUrls: ['./lesson-detail-page.component.scss']
})
export class LessonDetailPageComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild('date') dateRef: ElementRef
    @ViewChild('slugRef') slugRef: ElementRef
    @ViewChild('timeRef') timeRef: ElementRef
    @ViewChild('subjectRef') subjectRef: ElementRef
    @ViewChild('weekdayRef') weekdayRef: ElementRef

    form: FormGroup
    visitForm: FormGroup

    timeM: MaterialTimePicker
    dateM : MaterialDatePicker
    slugM: MaterialInstance
    subjectM: MaterialInstance
    weekDayM: MaterialInstance

    lessonsSub: Subscription
    subjectsSub: Subscription
    duplicateSub: Subscription

    lesson: LessonInDB
    slugs: string[]
    subjects: ScheduleSubject[]

    weekDays: WeekDayOption[] = weekDaysList

    // visit form
    isScheduling: boolean = false
    visitSub: Subscription


    // visit info
    private subscriber: Subscriber<null>
    fetchData$: Observable<any> = new Observable<null>(subscriber => this.subscriber = subscriber)


    constructor(
        private scheduleSubjectService: ScheduleSubjectService,
        private lessonService: LessonService,
        private route: ActivatedRoute,
        private router: Router,
        private visitService: VisitService,
    ) { }

    ngOnInit(): void {

        this.form = new FormGroup({
            subject: new FormControl({value: null, disabled: true}, Validators.required),
            slug: new FormControl({value: null, disabled: true}, Validators.required),
            time: new FormControl({value: null, disabled: true}, Validators.required),
            weekday: new FormControl({value: null, disabled: true}, Validators.required)
        })

        this.visitForm = new FormGroup({
            date: new FormControl(formatDate(new Date(), 'yyyy-MM-dd', 'en'), Validators.required),
        })

        this.form.disable()
        this.route.params.pipe(
            switchMap((params: Params) => {
                return this.lessonService.getById(params['id'])
            }),
        ).subscribe(
            lesson => {
                this.lesson = lesson
                this.form.patchValue({
                    subject: lesson.subject.name,
                    slug: lesson.week_slug,
                    weekday: lesson.weekday,
                    time: `${lesson.time.getHours()}:${lesson.time.getMinutes()}`,
                })
                this.form.enable()
            },
            () => MaterialService.toast('Невдається завантажити заняття.'),
        )

    }

    ngAfterViewInit(): void {
        this.subjectsSub = this.scheduleSubjectService.getAll().subscribe(
            subjects => {
                this.subjects = subjects
                setTimeout(() => {
                    this.subjectM = MaterialService.initFormSelect(this.subjectRef)
                }, 100)
            },
            error => MaterialService.toast(error.error.detail),
        )

        this.lessonsSub = this.lessonService.getAll().subscribe(
            lessons => {
                const slugs = [... new Set(lessons.map(l => l.week_slug))]
                this.slugM = MaterialService.initAutocomplete(this.slugRef, slugs, () => {
                    this.form.patchValue({
                        slug: this.slugM.el.value,
                    })
                })
            },
            error => MaterialService.toast(error.error.detail),
        )

        this.weekDayM = MaterialService.initFormSelect(this.weekdayRef)
        this.timeM = MaterialService.initTimePicker(this.timeRef, this.onCloseTimePicker.bind(this))

    }

    ngOnDestroy(): void {
        if (this.timeM) {
            this.timeM.destroy()
        }

        if (this.slugM) {
            this.slugM.destroy()
        }

        if (this.dateM) {
        this.dateM.destroy()
        }

        if (this.subjectM) {
            this.subjectM.destroy()
        }

        if (this.weekDayM) {
            this.weekDayM.destroy()
        }

        if (this.lessonsSub) {
            this.lessonsSub.unsubscribe()
        }

        if (this.subjectsSub) {
        this.subjectsSub.unsubscribe()
        }

        if (this.visitSub) {
            this.visitSub.unsubscribe()
        }

        if (this.duplicateSub) {
            this.duplicateSub.unsubscribe()
        }

    }

    onCloseTimePicker() {
        this.form.patchValue({
            time: this.timeM.time
        })
    }

    submit() {
        this.form.disable()

        const lesson: Lesson = {
            subject_name: this.form.value.subject,
            week_slug: this.form.value.slug,
            time: this.form.value.time,
            weekday: this.form.value.weekday,
        }

        this.lessonService.update(this.lesson.id, lesson).subscribe(
            lesson => {
                this.lesson = lesson
                this.form.enable()
            },
            error => MaterialService.toast(error.error.detail),
            () => {
                MaterialService.toast('Збережено')
            }
        )

    }

    delete() {
        this.lessonService.delete(this.lesson.id).subscribe(
            () => MaterialService.toast('Видалено'),
            error => MaterialService.toast(error.error.detail),
            () => this.router.navigate(['/lesson'])
        )
    }

    planeVisit() {
        this.isScheduling = true
        this.form.disable()
        this.weekDayM = MaterialService.initFormSelect(this.weekdayRef)
        this.subjectM = MaterialService.initFormSelect(this.subjectRef)

        setTimeout(() => {
            MaterialService.updateTextInput()
            this.dateM = MaterialService.initDatePicker(this.dateRef, () => {
                this.visitForm.patchValue({
                    date: formatDate(this.dateM.date, 'yyyy-MM-dd', 'en'),
                })
            })

        }, 100)

    }

    cancelScheduling() {
        this.form.enable()
        this.isScheduling = false
        this.weekDayM = MaterialService.initFormSelect(this.weekdayRef)
        this.subjectM = MaterialService.initFormSelect(this.subjectRef)
    }

    scheduleVisit () {
        this.visitForm.disable()

        const visit: Visit = {
            lesson_id: this.lesson.id,
            date: this.visitForm.value.date,

        }

        this.visitSub = this.visitService.visitLesson(visit).subscribe(
            visitInfo => {
                MaterialService.toast(`Візит заплановано. ID: ${visitInfo.id}`)
                this.visitForm.enable()
                this.cancelScheduling()
                this.subscriber.next(null)
            },
            error => {
                MaterialService.toast(error.error.detail)
                this.visitForm.enable()
            },
        )
    }

    duplicate() {
        this.form.disable()
        this.duplicateSub = this.lessonService.duplicate(this.lesson.id).subscribe(
            lesson => {
                this.form.enable()
                MaterialService.toast('Скопійовано')
                this.router.navigate(['/lesson', lesson.id])
            },
            () => {
                this.form.enable()
                MaterialService.toast('Помилка')
            }
        )
    }
}
