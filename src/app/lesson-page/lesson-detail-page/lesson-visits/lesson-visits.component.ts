import {Component, Input, OnInit, OnDestroy} from '@angular/core';
import {Observable, Subscription} from 'rxjs';

import {VisitInfo, VisitStatus} from '../../../shared/interfaces/visit-interfaces';
import {VisitService} from '../../../shared/services/visit.service';
import {MaterialService} from '../../../shared/services/material.service';

@Component({
    selector: 'app-lesson-visits',
    templateUrl: './lesson-visits.component.html',
    styleUrls: ['./lesson-visits.component.scss']
})
export class LessonVisitsComponent implements OnInit, OnDestroy {

    @Input('lesson_id') lesson_id?: number
    @Input('fetchData$') fetchData$?: Observable<null>

    fetchDataSub: Subscription
    visitsInfo: VisitInfo[] = []
    visitsSub: Subscription
    loading: boolean = false

    constructor(
        private visitService: VisitService,
    ) { }

    ngOnDestroy(): void {
        if (this.fetchDataSub) {
            this.fetchDataSub.unsubscribe()
        }

        if (this.visitsSub) {
            this.visitsSub.unsubscribe()
        }
    }

    ngOnInit(): void {
        this.fetchVisitsInfo()

        if (this.fetchData$) {
            this.fetchDataSub = this.fetchData$.subscribe(
                () => this.fetchVisitsInfo(),
                error => MaterialService.toast(error.error.toString()),
            )
        }

    }

    get statuses(): typeof VisitStatus {
        return VisitStatus
    }

    fetchVisitsInfo() {
        this.loading = true
        this.visitsSub = this.visitService.getVisitsInfo(this.lesson_id).subscribe(
            visitsInfo => {
                this.visitsInfo = visitsInfo
                this.loading = false
            },
            () => {
                MaterialService.toast('Помилка при завантаженні відвідувань')
                this.loading = false
            },
        )
    }

    deleteVisit(id: number) {
        this.visitService.deleteVisit(id).subscribe(
            () => {
                const index = this.visitsInfo.findIndex(v => v.id === id);
                this.visitsInfo.splice(index, 1);
                MaterialService.toast(`ID ${id} Видалено`)
            },
            () => MaterialService.toast('Виникнула помилка при видаленні'),
        )
    }

}
