import {WeekDays} from '../../shared/interfaces/schedule-interfaces';


export interface Filter {
    weekday?: WeekDays,
    weekSlug?: string,
}
