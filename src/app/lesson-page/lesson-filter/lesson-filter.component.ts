import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subscription} from 'rxjs';

import {Filter} from './filter';
import {weekDaysList} from '../../shared/conts';
import {LessonService} from '../../shared/services/lesson.service';
import {WeekDays} from '../../shared/interfaces/schedule-interfaces';
import {WeekDayOption} from '../../shared/interfaces/common-interfaces';
import {MaterialAutocomplete, MaterialInstance, MaterialService} from '../../shared/services/material.service';


@Component({
    selector: 'app-lesson-filter',
    templateUrl: './lesson-filter.component.html',
    styleUrls: ['./lesson-filter.component.scss']
})
export class LessonFilterComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('slugRef') slugRef: ElementRef
    @ViewChild('weekdayRef') weekdayRef: ElementRef

    @Output() onFilter = new EventEmitter<Filter>()

    @Input('weekday') weekday: WeekDays
    @Input('weekSlug') slug: string

    weekdayM: MaterialInstance
    slugM: MaterialAutocomplete

    lessonsSub: Subscription

    constructor(
        private http: HttpClient,
        private lessonService: LessonService,
    ) { }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.weekdayRef.nativeElement.value = this.weekday
        this.weekdayM = MaterialService.initFormSelect(this.weekdayRef)

        this.lessonsSub = this.lessonService.getAll().subscribe(
            lessons => {
                const slugs = [... new Set(lessons.map(l => l.week_slug))]
                this.slugM = MaterialService.initAutocomplete(this.slugRef, slugs, () => {
                    this.slug = this.slugM.el.value
                })
            },
            error => MaterialService.toast(error.error.detail),
        )

    }

    ngOnDestroy(): void {
        if (this.weekdayM) {
            this.weekdayM.destroy()
        }

        if (this.slugM) {
            this.slugM.destroy()
        }

        if (this.lessonsSub) {
            this.lessonsSub.unsubscribe()
        }

    }

    get weekdays(): WeekDayOption[] {
        return weekDaysList
    }

    submitFilter () {

        const filter: Filter = { }

        if (this.weekday) {
            filter.weekday = this.weekday
        }

        if (this.slug) {
            filter.weekSlug = this.slug
        }

        this.onFilter.emit(filter)
    }

    cancelFilter() {
        this.weekday = null
        this.submitFilter()
    }

    selected(weekday: string): boolean {
        return this.weekday === weekday
    }

}
