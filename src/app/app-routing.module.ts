import { NgModule } from '@angular/core'
import {RouterModule, Routes} from '@angular/router';

import {BaseLayoutComponent} from './shared/components/base-layout/base-layout.component'
import {SubjectPageComponent} from './subject-page/subject-page.component'
import {SubjectFormComponent} from './subject-page/subject-form/subject-form.component'
import {LessonPageComponent} from './lesson-page/lesson-page.component'
import {LessonFormComponent} from './lesson-page/lesson-form/lesson-form.component'
import {LessonDetailPageComponent} from './lesson-page/lesson-detail-page/lesson-detail-page.component';
import {LoginPageComponent} from './auth/login-page/login-page.component';
import {AuthGuard} from './shared/guards/auth.guard';
import {RegisterPageComponent} from './auth/register-page/register-page.component';
import {OverviewComponent} from './overview/overview.component';
import {ProfilePageComponent} from './profile-page/profile-page.component';

const routes: Routes = [
    {
        path: '',
        component: BaseLayoutComponent,
        children: [
            { path: '', redirectTo: '/login', pathMatch:'full' },
            { path: 'login', component: LoginPageComponent },
            { path: 'register', component: RegisterPageComponent },
        ]
    },
    {
        path: '',
        canActivate: [AuthGuard],
        component: BaseLayoutComponent,
        children: [
            { path: 'overview', component: OverviewComponent },
            { path: 'profile', component: ProfilePageComponent },

            { path: 'subject', component: SubjectPageComponent },
            { path: 'subject/new', component: SubjectFormComponent },
            { path: 'subject/:name', component: SubjectFormComponent },

            { path: 'lesson', component: LessonPageComponent },
            { path: 'lesson/new', component: LessonFormComponent },
            { path: 'lesson/:id', component: LessonDetailPageComponent },
        ]
    },
]


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule { }
