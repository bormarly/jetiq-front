import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {Clipboard} from '@angular/cdk/clipboard'

import {User} from '../shared/interfaces/auth-interfaces';
import {AuthService} from '../shared/services/auth.service';
import {MaterialService} from '../shared/services/material.service';


@Component({
    selector: 'app-profile-page',
    templateUrl: './profile-page.component.html',
    styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit, OnDestroy {

    user: User
    userSub: Subscription

    constructor(
        private auth: AuthService,
        private clipboard: Clipboard,
    ) { }

    ngOnDestroy(): void {
        if (this.userSub) {
            this.userSub.unsubscribe()
        }
    }

    ngOnInit(): void {
        this.userSub = this.auth.getCurrentUser().subscribe(
            user => this.user = user,
            error => MaterialService.toast(error.error.detail)
        )
    }

    copyEmail() {
        if (this.user) {
            this.clipboard.copy(this.user.email)
            MaterialService.toast('Скопійовано')
        }
    }

}
