import {WeekDays} from './interfaces/schedule-interfaces';
import {WeekDayOption} from './interfaces/common-interfaces';


export const weekDaysList: WeekDayOption[] = [
    { label: `Неділя` , value: WeekDays.SUNDAY },
    { label: 'Понеділок' , value: WeekDays.MONDAY },
    { label: 'Вівторок' , value: WeekDays.TUESDAY },
    { label: 'Середа' , value: WeekDays.WEDNESDAY },
    { label: 'Четверг' , value: WeekDays.THURSDAY },
    { label: `П'ятниця` , value: WeekDays.FRIDAY },
    { label: `Субота` , value: WeekDays.SATURDAY },
]


export function getCurrentWeekDay(): WeekDayOption {
    return weekDaysList[new Date().getDay()]
}
