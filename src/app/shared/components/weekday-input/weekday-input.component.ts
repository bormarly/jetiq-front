import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {WeekDayOption} from '../../interfaces/common-interfaces';
import {getCurrentWeekDay, weekDaysList} from '../../conts';

@Component({
    selector: 'app-weekday-input',
    templateUrl: './weekday-input.component.html',
    styleUrls: ['./weekday-input.component.scss']
})
export class WeekdayInputComponent implements OnInit, AfterViewInit {

    @Output('choseEvent') choseEvent = new EventEmitter<string>();

    currentValue: string = getCurrentWeekDay().value

    constructor() { }

    ngAfterViewInit(): void {
        this.chose(this.currentValue)
    }


    ngOnInit(): void {
    }


    get weekdays() : WeekDayOption[] {
        return weekDaysList
    }

    chose(weekday: string) {
        this.currentValue = weekday
        this.choseEvent.emit(weekday)
    }
}
