import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MaterialInstance, MaterialService} from '../../services/material.service';

@Component({
    selector: 'app-material-box',
    templateUrl: './material-box.component.html',
    styleUrls: ['./material-box.component.scss']
})
export class MaterialBoxComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('ref') ref: ElementRef

    @Input('imgSrc') imgSrc: string
    @Input('alt') alt: string = ''
    @Input('width') width: number = 300
    @Input('height') height: number = 300

    materialBox: MaterialInstance

    constructor() { }

    ngOnInit(): void {
    }

    ngAfterViewInit(): void {
        this.materialBox = MaterialService.initMaterialBox(this.ref)
    }

    ngOnDestroy(): void {
        this.materialBox.destroy()
    }

}
