import { Component, OnInit } from '@angular/core'
import {Router} from '@angular/router';

import {LinkHTML} from '../../interfaces/common-interfaces';
import {AuthService} from '../../services/auth.service';
import {MaterialService} from '../../services/material.service';


@Component({
    selector: 'app-base-layout',
    templateUrl: './base-layout.component.html',
    styleUrls: ['./base-layout.component.scss']
})
export class BaseLayoutComponent implements OnInit {


    private authLinks: LinkHTML[] = [
        { routerLink: ['/subject'], label: 'Предмети' },
        { routerLink: ['/lesson'], label: 'Заняття' },
        { routerLink: ['/profile'], label: 'Профіль' },
    ]
    private notAuthLinks = [
        { routerLink: ['/register'], label: 'Зареєструватись' },
        { routerLink: ['/login'], label: 'Увійти' },
    ]

    constructor(
        public auth: AuthService,
        private router: Router,
    ) { }

    ngOnInit(): void { }

    get navLinks(): LinkHTML[] {
        if (this.auth.isAuthenticated()) {
            return this.authLinks
        } else {
            return this.notAuthLinks
        }
    }

    logout() {
        this.auth.logout()
        this.router.navigate(['/login'])
        MaterialService.toast('Вихід')
    }
}
