import { Injectable } from '@angular/core'
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Visit, VisitInfo} from '../interfaces/visit-interfaces';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class VisitService {

    private visitLessonURL: string = `${environment.scheduleApiURL}/visit/lesson`
    private visitInfoURL: string = `${environment.scheduleApiURL}/visit/visit_status`
    private visitsInfoURL: string = `${environment.scheduleApiURL}/visit/visit_statuses`

    constructor(
        private http: HttpClient,
    ) { }

    visitLesson(visit: Visit): Observable<VisitInfo> {
        return this.http.post<VisitInfo>(this.visitLessonURL, visit)
    }

    getVisitsInfo(lesson_id?: number): Observable<VisitInfo[]> {

        let params = new HttpParams()

        if (lesson_id) {
            params = params.set('lesson_id', String(lesson_id))
        }

        return this.http.get<VisitInfo[]>(`${this.visitsInfoURL}`, { params })
    }

    deleteVisit(id: number): Observable<void> {
        return this.http.delete<void>(`${this.visitInfoURL}/${id}`)
    }

}
