import { Observable } from 'rxjs'
import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

import { environment } from '../../../environments/environment'
import { ScheduleSubject } from '../interfaces/schedule-interfaces'


@Injectable({
    providedIn: 'root'
})
export class ScheduleSubjectService {

    private subjectURL: string = `${environment.scheduleApiURL}/schedule/subject`

    constructor(private http: HttpClient) { }

    getAll(): Observable<ScheduleSubject[]> {
        return this.http.get<ScheduleSubject[]>(this.subjectURL)
    }

    create (subject: ScheduleSubject): Observable<ScheduleSubject> {
        return this.http.put<ScheduleSubject>(this.subjectURL, subject)
    }

    getByName(name: string): Observable<ScheduleSubject> {
        return this.http.get<ScheduleSubject>(`${this.subjectURL}/${name}`)
    }

    update(name: string, subject: ScheduleSubject): Observable<ScheduleSubject> {
        return this.http.patch<ScheduleSubject>(`${this.subjectURL}/${name}`, subject)
    }

    delete(name: string): Observable<void> {
        return this.http.delete<void>(`${this.subjectURL}/${name}`)
    }

}
