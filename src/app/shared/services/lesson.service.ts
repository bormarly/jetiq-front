import { Injectable } from '@angular/core'
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Lesson, LessonInDB, WeekDays} from '../interfaces/schedule-interfaces';
import {map} from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class LessonService {

    private lessonURL: string = `${environment.scheduleApiURL}/schedule/lesson`

    constructor(private http: HttpClient) { }

    getAll(weekDay?: WeekDays | string, weekSlug?: string): Observable<LessonInDB[]> {

        let params = new HttpParams()

        if (weekDay) {
            params = params.set('weekday', weekDay)
        }

        if (weekSlug) {
            params = params.set('week_slug', weekSlug)
        }


        return this.http.get<LessonInDB[]>(this.lessonURL, { params })
            .pipe(
                map(lessons => lessons.map(LessonService.prepareLesson)),
            )
    }

    getById(id: number): Observable<LessonInDB> {
        return this.http.get<LessonInDB>(`${this.lessonURL}/${id}`)
            .pipe(
                map(LessonService.prepareLesson),
            )
    }

    create(lesson: Lesson): Observable<LessonInDB> {
        return this.http.put<LessonInDB>(this.lessonURL, lesson)
            .pipe(
                map(LessonService.prepareLesson),
            )
    }

    update(id: number, lesson: Lesson): Observable<LessonInDB> {
        return this.http.patch<LessonInDB>(`${this.lessonURL}/${id}`, lesson)
            .pipe(
                map(LessonService.prepareLesson),
            )
    }

    delete(id: number): Observable<void> {
        return this.http.delete<void>(`${this.lessonURL}/${id}`)
    }

    private static prepareLesson(lesson: LessonInDB): LessonInDB {
        lesson.time = new Date(`3000/1/1 ${lesson.time}`)
        return lesson
    }

    duplicate(id: number): Observable<LessonInDB> {
        return this.http.post<LessonInDB>(`${this.lessonURL}/${id}/copy`, {})
    }
}
