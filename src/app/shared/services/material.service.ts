import {ElementRef, Injectable} from '@angular/core';
import {formatDate} from '@angular/common';

declare var M


export interface MaterialInstance {
    open?(): void
    close?(): void
    destroy?(): void
    el?: any,
}


export interface MaterialTimePicker extends MaterialInstance {
    time: string
    isOpen: boolean
}

export interface MaterialDatePicker extends MaterialInstance {
    date?: Date,
}

export interface MaterialAutocomplete extends MaterialInstance {
    activeIndex: number,
}


@Injectable({
    providedIn: 'root'
})
export class MaterialService {

    constructor() { }

    public static toast(message: string) {
        M.toast({html: message})
    }

    static initFormSelect(ref: ElementRef): MaterialInstance {
        return M.FormSelect.init(ref.nativeElement)
    }

    static initTimePicker(ref: ElementRef, onCloseEnd:() => void): MaterialTimePicker {
        return M.Timepicker.init(ref.nativeElement, {
            showClearBtn: true,
            twelveHour: false,
            onCloseEnd,
        })
    }

    static initAutocomplete(ref: ElementRef, data: string[], onAutocomplete: () => void): MaterialAutocomplete {

        let autocompleteData = {}

        data.forEach(item => {
            autocompleteData[item] = null
        })

        return M.Autocomplete.init(ref.nativeElement, {
            data: autocompleteData,
            onAutocomplete,
        })
    }

    static updateTextInput() {
        M.updateTextFields()
    }

    static initDatePicker(ref: ElementRef, onClose:() => void): MaterialDatePicker {
        return M.Datepicker.init(ref.nativeElement, {
            format: 'yyyy-mm-dd',
            showClearBtn: true,
            onClose,
            defaultDate: formatDate(new Date(), 'yyyy-mm-dd', 'en')
        })
    }

    static initMaterialBox(ref: ElementRef): MaterialInstance {
        return M.Materialbox.init(ref.nativeElement)
    }

}
