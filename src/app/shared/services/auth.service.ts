import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Token, User} from '../interfaces/auth-interfaces';


@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private token?: Token = null
    private authURL: string = `${environment.scheduleApiURL}/auth`

    constructor(private http: HttpClient) { }

    register(user: User): Observable<User> {
        return this.http.put<User>(`${this.authURL}/register`, user)
    }

    login(formData: FormData): Observable<Token> {
        return this.http.post<Token>(`${this.authURL}/login`, formData).pipe(
            tap(token => {
                AuthService.saveTokenToStorage(token)
                this.setToken(token)
            }),
        )
    }

    setToken(token: Token) {
        this.token = token
    }

    getToken(): Token {
        return this.token
    }

    isAuthenticated(): boolean {
        return !!this.token
    }

    logout() {
        this.setToken(null)
        localStorage.clear()
    }

    getCurrentUser(): Observable<User> {
        return this.http.get<User>(`${this.authURL}/user`)
    }

    static loadTokenFromStorage(): Token | null {
        const token = localStorage.getItem('auth-token')
        const type = localStorage.getItem('token-type')

        if (token && type) {

            return {
                access_token: token,
                token_type: type,
            }

        } else {
            return null
        }

    }

    private static saveTokenToStorage(token: Token) {
        localStorage.setItem('auth-token', token.access_token)
        localStorage.setItem('token-type', token.token_type)
    }



}
