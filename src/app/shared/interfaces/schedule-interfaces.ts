
export interface ScheduleSubject {
    name: string,
    teacher: string,
    meet_url_name: string,
}


export enum WeekDays {
    MONDAY = 'monday',
    TUESDAY = 'tuesday',
    WEDNESDAY = 'wednesday',
    THURSDAY = 'thursday',
    FRIDAY = 'friday',
    SATURDAY = 'saturday',
    SUNDAY = 'sunday',
}


interface BaseLesson {
    time: Date,
    weekday: WeekDays | string,
    week_slug: string,
}


export interface Lesson extends BaseLesson {
    id?: number,
    subject_name: string,
}


export interface LessonInDB extends BaseLesson {
    id: number,
    subject: ScheduleSubject,
}
