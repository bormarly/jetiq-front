export interface User {
    email?: string,
    username: string,
    password?: string,
    jetiq_username: string,
    jetiq_password?: string,
}

export interface Token {
    access_token: string,
    token_type: string,
}

export interface LoginBody {
    body: User
}
