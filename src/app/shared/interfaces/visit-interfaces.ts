
export interface Visit {
    date: string,
    lesson_id: number,
}


export enum VisitStatus {
    CREATED = 'created',
    RUNNING = 'running',
    SUCCESSFUL = 'successful',
    FAILED = 'failed',
}

export interface VisitInfo {
    id: number,
    date: Date,
    lesson_id: number,
    status: VisitStatus,
    error_message?: string,
    visit_start?: Date,
    visit_finish?: Date,
    image?: string,
}
