export interface WeekDayOption {
    label: string
    value: string
}


export interface LinkHTML {
    label: string,
    routerLink: string[],
}
