import { Pipe, PipeTransform } from '@angular/core';
import {environment} from '../../../environments/environment';

@Pipe({
  name: 'mediaURL'
})
export class MediaURLPipe implements PipeTransform {

    private path: string = `${environment.scheduleApiURL}/media`

    transform(mediaUrl: string): string {
        return `${this.path}/?path=${mediaUrl}`
    }

}
