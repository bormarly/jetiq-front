import { Pipe, PipeTransform } from '@angular/core';
import {weekDaysList} from '../conts';

@Pipe({
    name: 'weekdayUA'
})
export class WeekdayPipe implements PipeTransform {

    transform(weekday: string): string {
        return weekDaysList.find(weekDayOption => weekDayOption.value == weekday).label
    }

}
