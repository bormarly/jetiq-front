import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {TitleCasePipe} from '@angular/common';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(
        private auth: AuthService,
        private router: Router,
        private titlePipe: TitleCasePipe,

    ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.auth.isAuthenticated()) {
            const token = this.auth.getToken()
            request = request.clone({
                setHeaders: {
                    Authorization: `${this.titlePipe.transform(token.token_type)} ${token.access_token}`
                }
            })
        }

        return next.handle(request).pipe(
            catchError((err: HttpErrorResponse) => this.handleAuthError(err))
        )

    }

    private handleAuthError(err: HttpErrorResponse): Observable<any> {
        if (err.status === 401) {
            this.router.navigate(['/login'], {
                queryParams: {
                    sessionFailed: true,
                }
            })
        }
        return throwError(err)
    }
}
